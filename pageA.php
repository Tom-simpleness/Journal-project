<?php
require ('connexion.php');
session_start();
$IDA = $_GET['id'];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Journal</title>
    <link rel="stylesheet" href="style2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
<header>
    <div class="topnav">
        <a class="active" href="index1.php">Home</a>
        <a href="add.php"><i class="fas fa-plus-circle"></i></a>
        <a><i class="fas fa-users right"></i></a>
        <?php if(isset($_SESSION['pseudo']))
        {
            if(!empty($_SESSION['pseudo']))
            {
                echo  ('<a>'.($_SESSION['pseudo']).'</a><a href="deconnexion.php">Déconnexion</a>');
            }
            else
            {
                echo ('Se connecter?');
            }
        }
        ?>
    </div>

</header>
<?php
echo ("<p id='hidden'>".$IDA."</p>");
?>
</body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="ajax2.js"></script>
</body>

</html>