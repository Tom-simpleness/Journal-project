<?php
require ('connexion.php');
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Journal</title>
    <link rel="stylesheet" href="style1.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
<header>
    <div class="topnav">
        <a class="active" href="index1.php">Home</a>
        <a href="add.php"><i class="fas fa-plus-circle"></i></a>
        <a href="loglog.php"><i class="fas fa-users right"></i></a>
        <?php if(isset($_SESSION['pseudo']))
        {
            if(!empty($_SESSION['pseudo']))
            {
                echo  ('<a>'.($_SESSION['pseudo']).'</a><a href="deconnexion.php">Déconnexion</a>');
            }
            else
            {
                echo ('Se connecter?');
            }
        }
        ?>
    </div>

</header>
<div id="dispo">
<form action="" method="POST">
    <label for="titre">Titre</label></br>
    <input type="text" value="" id="Titre" name="Titre" required></br>
    <label for="content">Article</label></br>
    <textarea name="Content" id="Content" required></textarea></br>
    <label for="Date"></label>Date</br>
    <input type="date" name="Ladate" id="Ladate" required></br>
    <label for="Nom">Auteur</label></br>
    <input type="text" name="NomA" id="NomA" required></br>
    <input type="submit" id="submit" value="POSTER">
</form>
</div>

<?php
if(isset($_POST["Titre"])) {
    $titre = htmlspecialchars($_POST['Titre']);
    $content = htmlspecialchars($_POST['Content']);
    $ladate = htmlspecialchars($_POST['Ladate']);
    $nomA = htmlspecialchars($_POST['NomA']);

    $req_pre = mysqli_prepare($link, 'INSERT INTO Articles(Titre, Content, Ladate, NomA) VALUES ( ?,?, ?, ?)');

    mysqli_stmt_bind_param($req_pre, "ssss", $titre, $content, $ladate, $nomA);

    mysqli_stmt_execute($req_pre);
}
?>
</body>
</html>