<?php
require ('connexion.php');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Journal</title>
    <link rel="stylesheet" href="style1.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
<header>
    <div class="topnav">
        <a class="active" href="index1.php">Home</a>
        <a href="add.php"><i class="fas fa-plus-circle"></i></a>
        <a href="loglog.php"><i class="fas fa-users right"></i></a>
    </div>
</header>
<a href="connection.php"><i id="loglog" class="fas fa-user-circle"></i></a><a href="inscription.php"><i id="signIn" class="fas fa-sign-in-alt"></i></a>
</body>
</html>