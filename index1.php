<?php
require ('connexion.php');
session_start();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Journal</title>
    <link rel="stylesheet" href="style1.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
<header>
    <div class="topnav">
        <a class="active" href="index1.php">Home</a>
        <a href="add.php"><i class="fas fa-plus-circle"></i></a>
        <a href="loglog.php"><i class="fas fa-users right"></i></a>
        <?php if(isset($_SESSION['pseudo']))
    {
        if(!empty($_SESSION['pseudo']))
        {
            echo  ('<a>'.($_SESSION['pseudo']).'</a><a href="deconnexion.php">Déconnexion</a>');
        }
        else
        {
            echo ('Se connecter?');
        }
    }
        ?>

    </div>


</header>
<script src="ajax.js"></script>
</body>
</html>