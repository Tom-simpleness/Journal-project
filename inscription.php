<?php
$link = mysqli_connect("localhost", "root", "00000", "Exo3");
if ($link->connect_errno) {
    printf("Échec de la connexion : %s\n", $mysqli->connect_error);
    exit();
}
else {
    echo '';
}
if (isset($_POST['forminscription'])) {

    $pseudo = htmlspecialchars($_POST['pseudo']);
    $mail = htmlspecialchars($_POST['mail']);
    $password = sha1($_POST['password']);
    $Vpassword = sha1($_POST['Vpassword']);

    $pseudolength = strlen($pseudo);

    if (!empty($_POST['pseudo']) AND !empty($_POST['password']) AND !empty($_POST['Vpassword']) AND !empty($_POST['mail'])) {

        if ($pseudolength <= 255) {
            if (filter_var($mail, FILTER_VALIDATE_EMAIL)){
                if ($password == $Vpassword){

                    $req_pre = mysqli_prepare($link, 'INSERT INTO UserInfo(pseudo, pass, email) VALUES (?,?,?)');

                    mysqli_stmt_bind_param($req_pre, "sss", $pseudo, $password, $mail);
                    mysqli_stmt_execute($req_pre);

                   } else {
                    echo $Vpassword.'   '.$password;
                  }
            } else {
                $erreur = 'mail WRONG';
            }
        } else {
            $erreur = 'pseudo trop long';
        }


    } else {
        $erreur = "champs vident";
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Journal</title>
    <link rel="stylesheet" href="style1.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<body>
<header>
    <div class="topnav">
        <a class="active" href="index1.php">Home</a>
        <a href="add.php"><i class="fas fa-plus-circle"></i></a>
        <a href="loglog.php"><i class="fas fa-users right"></i></a>
    </div>
</header>
<body>
<form method="POST">
<table>
<tr>
    <td><label for="">Pseudo</label></td>
    <td><input type="text" name="pseudo" id="pseudo" value="<?php if(isset($pseudo)){echo $pseudo;} ?>"></td>
</tr>
<tr>
    <td><label for="">Mot 2 passe</label></td>
    <td><input type="password" name="password" id="password"></td>
</tr>
<tr>
    <td><label for="">Verif MDP</label></td>
    <td><input type="password" name="Vpassword" id="Vpassword"></td>
</tr>
<tr>
    <td><label for="">Mail</label></td>
    <td><input type="text" name="mail" id="mail"></td>
</tr>
    <tr>
        <td><input type="submit" value="s'inscrire" name="forminscription"></td>
    </tr>

</table>
</form>
<?php
if (isset($erreur)){
    echo '<p style="color:red">'.$erreur.'</p>';
}
?>
</body>
</html>